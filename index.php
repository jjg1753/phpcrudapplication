<!DOCTYPE html>
<html>
  <head>
    <title>
      PHP CRUD APPLICATION
    </title>

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </head>

  <body>
    <?php require_once 'process.php';?>

    <?php

    if(isset($_SESSION['message'])):?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>">

    <?php
    echo $_SESSION['message'];
    unset($_SESSION['message']);
     ?>
   </div>
 <?php endif ?>
    <div class = "container">
    <?php
    $mysqli = new mysqli('localhost', 'root', 'Zg5cws26fnY3Loy3', 'phpcrud') or die(mysqli_error($mysqli));
    $result = $mysqli->query("SELECT * from data_crud") or die($mysqli -> error);
    #print_r($result->fetch_assoc());
    ?>

    <div class="row justify-content-center">
      <table class="table">
        <thead>
          <tr>
            <th>
              Name
            </th>
            <th>
              Location
            </th>
            <th colspan="2">
            Actions

            </th>
          </tr>
        </thead>
        <?php
          while($row = $result->fetch_assoc()): ?>
            <tr>
              <td>
                <?php echo $row['name']; ?>
              </td>
              <td>
                <?php echo $row['location']; ?>
              </td>
              <td>
                <a href="index.php?edit=<?php echo $row['primary_id']; ?>"
                  class="btn btn-info">Edit</a>
                <a href="process.php?delete=<?php echo $row['primary_id']; ?>"
                    class="btn btn-danger">X</a>
              </td>
            </tr>
          <?php endwhile; ?>
      </table>
    </div>
    <div class="row justify-content-center">
      <form action="process.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label>Name</label>
        <input type = "text" name="name" value="<?php echo $name; ?>" class="form-control" placeholder="Enter your name" />
      </div>
      <div class="form-group">
        <label>Location</label>
        <input type = "text" name="location" value="<?php echo $location; ?>" class="form-control" placeholder="Enter your location" />
      </div>
      <div class="form-group">
        <?php if($update == true): ?>
        <button type ="submit" class="btn btn-primary" name="update">Update</button>
      <?php else: ?>
        <button type ="submit" class="btn btn-primary" name="save">Save</button>
      <?php endif; ?>
      </div>
      </form>
    </div>
  </div>
  </body>
</html>
